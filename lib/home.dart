import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import './main.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/services.dart';
import 'dart:convert';
import 'package:path_provider/path_provider.dart';
import 'dart:async';
import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:shared_preferences/shared_preferences.dart';
import './about.dart';
import './file_picker.dart';


class HomePage extends StatefulWidget {
  var name;
  var mob;
  var email;
	@override
	_HomePageState createState() => _HomePageState(name,mob,email);
}

class _HomePageState extends State<HomePage> {
  SharedPreferences sharedPreferences;
  bool _isLoading = true;
  var name;
  var mob;
  var email;
  int _selectedPage = 0;
  final _pageOptions = [
    Text('Home'),
    Text('Work'),
    Text('Landscape'),
  ];

  _HomePageState(this.name,this.mob,this.email);

  @override
  void initState() {
    super.initState();
    read();
  }

  Future<Null> logout() async {
    final directory = await getApplicationDocumentsDirectory();
    final file = File('${directory.path}/my_file.txt');
    await file.delete();
    print(file);
    setState(() {
      name = '';
      _isLoading = false;
    });
    Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (BuildContext context) => MainPage()), (Route<dynamic> route) => false);
  }

  Future<void> read() async {
    try {
      final directory = await getApplicationDocumentsDirectory();
      final file = File('${directory.path}/my_file.txt');
      String text = await file.readAsString();
      var dict = json.decode(text);
      setState(() {
        name = dict['data']['username'];
        mob = dict['data']['mobile'];
        email = dict['data']['email'];
      });
      } catch (e) {
        print("Couldn't read file");
      }
    } 


  @override
  Widget build(BuildContext context) {
    print(_isLoading);
		return new Scaffold(
			appBar: new AppBar(
				title: new Text('Home Page'),
				actions: <Widget>[
            FlatButton(
              onPressed: () {
                logout();
              },
              child: Text("Log Out", style: TextStyle(color: Colors.white)),
            ),
          ],
        ),
			body: Container(
        child: Column(
          children: <Widget>[
            Text('Name: $name'),
            Text('Mobile: $mob'),
            Text('Email: $email'),
          ],
          ),
        ),
      drawer: new Drawer(
        child: ListView(
          children: <Widget>[
            new UserAccountsDrawerHeader(
              accountName: new Text('$name'),
              accountEmail: new Text('$email'),
              currentAccountPicture: new CircleAvatar(
                backgroundImage: new NetworkImage('https://i.pravatar.cc/300'),
                )
              ),
              new ListTile(
                title: new Text('About Page'),
                onTap: () {
                  Navigator.of(context).pop();
                  Navigator.push(context, new MaterialPageRoute(
                    builder: (BuildContext context) => new AboutPage())
                    );
                },
                ),
              new ListTile(
                title: Text('File Upload'),
                onTap: () {
                  Navigator.of(context).pop();
                  Navigator.push(context, new MaterialPageRoute(
                    builder: (BuildContext context) => new FilePickerDemo())
                    );
                  },
                ), 
          ]
          ),
        ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _selectedPage,
        onTap: (int index) {
          setState(() {
            _selectedPage = index;
            }); 
          },
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text('Home') 
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.work),
            title: Text('Work') 
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.landscape),
            title: Text('Landscape') 
          ),
        ],
        ), 
			);
	}
}