import 'dart:async';
import 'package:flutter/material.dart';

import 'package:webview_flutter/webview_flutter.dart';

class MyWebView extends StatefulWidget {
	@override
	_MyWebViewState createState() => _MyWebViewState();
}

class _MyWebViewState extends State<MyWebView> {

	final Completer<WebViewController> _controller = Completer<WebViewController>();
	

	@override
	Widget build(BuildContext context) {
		return Scaffold(
			appBar: AppBar(
				title: Text('Browser'),
			),
			body: WebView(
        		initialUrl: "https://google.com",
        		onWebViewCreated: (WebViewController webViewController) {
          			_controller.complete(webViewController);
        },
        ),
	);		
	}
}