# Flutter app

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

# Installing flutter

1. Download Flutter SDK Archive: https://flutter.io/sdk-archive/#linux.

2. Extract the Flutter SDK. Navigate to the location of TAR file.

3. Add flutter/bin to PATH.To append complete path of flutter/bin to PATH variable, open .bash_profile  file in edit mode and add the following line at the end of the file.
For UBUNTU, add the complete path of [PATH_TO_FLUTTER_SDK_DIRECTORY]/flutter/bin to PATH in .bashrc  file.

	For Linux/Mac, use command:

		export PATH="[PATH_TO_FLUTTER_SDK_DIRECTORY]"

	For Windows, use command:

		set PATH="[PATH_TO_FLUTTER_SDK_DIRECTORY]"

4. Restart the terminal.

5. Flutter SDK Setup is complete.

# Android Studio Setup

Download latest Android Studio: https://developer.android.com/studio/index.html#downloads

This will install the latest Android SDK, Android SDK Platform-Tools, and Android SDK Build-Tools, which are required by Flutter when developing for Android.

## Install Flutter and Dart plugins:

To install Flutter and Dart plugins in Android Studio.

1. Open Android Studio and open an existing project or create a new one.

2. Under Menu -> File -> Settings -> Plugins, search for flutter and click on Search in repositories.

3. Click on Install button.

4. Click on Yes, to insert both Dart and Flutter plugins.

## Run Flutter Doctor Command

To check if the installation is successful, run **flutter doctor** command.

Flutter SDK installation is good.

It is not able to find Android SDK. We have Android SDK already installed, so we shall go with the option of setting $ANDROID_HOME. Add the following line at the end of .bashrc file.

For Linux/Mac, use command:

	export ANDROID_HOME="[PATH_TO_ANDROID_SDK_DIRECTORY]"

For Windows, use command:

	set ANDROID_HOME="[PATH_TO_ANDROID_SDK_DIRECTORY]"

Once all the issues are resolved, run **flutter command**.

## Create your first flutter app

https://alligator.io/flutter/your-first-flutter-app/




###### *Thanks for reading!*